import os
from skimage import measure
from skimage import io

def compare_image_quality(img_dir, img_truth_name, img_sr_name, img_compare_name):
    # load images
    img_truth = io.imread(img_dir + '\\' +  img_truth_name)
    img_sr = io.imread(img_dir + '\\' +  img_sr_name)
    img_compare = io.imread(img_dir + '\\' + img_compare_name)

    # calculate sr metrics
    sr_psnr = measure.compare_psnr(img_truth, img_sr)
    sr_ssim = measure.compare_ssim(img_truth, img_sr, multichannel=True)

    # calculate compare metrics
    compare_psnr = measure.compare_psnr(img_truth, img_compare)
    compare_ssim = measure.compare_ssim(img_truth, img_compare, multichannel=True)

    # return result
    print(img_dir)
    print('SR|Compare PSNR: ' + str(sr_psnr) + ' | ' + str(compare_psnr))
    print('SR|Compare SSIM: ' + str(sr_ssim) + ' | ' + str(compare_ssim))


dir_list = next(os.walk('.'))[1]

img_dir = 'cufed001\\'
img_truth_name = 'HR.png'
img_sr_name = 'SRNTT.png'
img_compare_name = 'Upscale.png'    

for dir_name in dir_list:
    compare_image_quality(dir_name, img_truth_name, img_sr_name, img_compare_name)


